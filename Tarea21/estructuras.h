struct auto
{
  char marca[50];
  char  modelo[25];
  char  anio[10];
};

struct computadora
{
  char marca[25];
  char modelo[25];
  int cantRAM[10];
  char procesador[50];
};
struct alumno
{
  int edad;
  char Nombre[60];
  char genero;
  char carrera[25];
  int nCuenta[10];
};
struct Equipo_de_futbol
{
  char nombre[45];
  char liga[45];
  char estrellaEquipo[45];
  char noTitulos;

};
struct Mascota
{
  char nombre[30];
  char reino[50];
  char genero[50];
  char familia[50];
  char filo[50];
  char especie[50];
};

struct videojuego
{
  char nombre[45];
  int anio;
  char desarrolladora[45];

};

struct superHeroe
{
  char nombre[45];
  char poderes[45];
  char universo[20];
};

struct planetas
{
  char nombre[45];
  char galaxia[45];
  char sistema[45];
  char tipo[45];
};

struct personajeVideojuego
{
  char nombre[45];
  char juego[45];
  char papel[45];
};

struct comic
{
  char nombre[45];
  char universo[45];
  char personajePrincipal[45];
  char noEntregas;
};
