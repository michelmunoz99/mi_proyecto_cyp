#include <stdio.h>
#include "FuncionM.h"
#include "FuncionI.h"
#include "FuncionC.h"
#include "FuncionH.h"
#include "FuncionE.h"
#include "FuncionL.h"

int main(int argc, char const *argv[])
{
  fnM();
  fnI();
  fnC();
  fnH();
  fnE();
  fnL();

  return 0;
}
